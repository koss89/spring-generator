package com.code.generator.main;

import com.code.generator.builders.FullBuilder;
import com.code.generator.generators.AbstractGenerator;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;

import java.io.*;
import java.util.Arrays;
import java.util.logging.Logger;

public class mainCL {

    String workDir = System.getProperty("user.dir");
    String BasePackage = "test";
    Logger log = Logger.getLogger("mainCL");

    public static void main(String[] args) throws Exception {

        mainCL mc = new mainCL(args);
    }

    public mainCL(String[] args) throws FileNotFoundException {
        if (args.length != 0 && args[0] != null) {
            if (args.length > 1) {
                if (args[1] != null) {
                    BasePackage = args[1];
                }
            }

            File workDir = new File(this.workDir + "/" + args[0]);
            if (workDir.isDirectory()) {
                Arrays.stream(workDir.listFiles()).forEach(file -> {
                    try {
                        GenerateAll(file);
                    } catch (FileNotFoundException e) {
                        log.severe(e.toString());
                    }
                });
            }
        } else {
            File fil = new File("/home/koss/DEVELOPMENT/PROJECTS/CCS/EntityLib/src/main/java/Ent/Vopros.java");
            GenerateAll(fil);

        }

    }

    void GenerateAll(File entityFile) throws FileNotFoundException {
        FileInputStream in = new FileInputStream(entityFile);
        CompilationUnit EntityUnit = JavaParser.parse(in);

        String repoPackName = "repository";
        String servicePackName = "service";
        String ctrlPackName = "rest";

        CompilationUnit RepoUnit = FullBuilder.getInstance()
                .setBasePackage(BasePackage)
                .setPackName(repoPackName)
                .genRepository(EntityUnit);

        saveFile(RepoUnit, workDir + "/" + repoPackName);
        log.info("-----------------------------------------------------------------------");

        CompilationUnit GenericUnit = FullBuilder.getInstance()
                .setBasePackage(BasePackage)
                .setPackName(servicePackName)
                .genGenericService();

        saveFile(GenericUnit, workDir + "/" + servicePackName);
        log.info("-----------------------------------------------------------------------");

        CompilationUnit ServiceUnit = FullBuilder.getInstance()
                .setBasePackage(BasePackage)
                .setPackName(servicePackName)
                .genService(EntityUnit, GenericUnit);
        saveFile(ServiceUnit, workDir + "/" + servicePackName);
        log.info("-----------------------------------------------------------------------");

        CompilationUnit ServiceImplUnit = FullBuilder.getInstance()
                .setBasePackage(BasePackage)
                .setPackName(servicePackName)
                .genServiceImpl(EntityUnit, ServiceUnit, RepoUnit);
        saveFile(ServiceImplUnit, workDir + "/" + servicePackName + "/" + "impl");
        log.info("-----------------------------------------------------------------------");

        CompilationUnit CtrlUnit = FullBuilder.getInstance()
                .setBasePackage(BasePackage)
                .setPackName(ctrlPackName)
                .genCtrl(EntityUnit, ServiceUnit);
        saveFile(CtrlUnit, workDir + "/" + ctrlPackName);
    }

    void saveFile(CompilationUnit cu, String dir) {
        File repoDir = new File(dir);
        if (!repoDir.exists()) {
            repoDir.mkdir();
        }
        AbstractGenerator abstractGenerator = new AbstractGenerator() {
            @Override
            public CompilationUnit generate() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
        String filePath = dir + "/" + abstractGenerator.getClassName(cu) + ".java";
        File file = new File(filePath);
        try {
            file.createNewFile();
            BufferedWriter writer = null;
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(cu.toString());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
