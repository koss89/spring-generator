package com.code.generator.generators;

import com.github.javaparser.ast.CompilationUnit;

public interface IGenerator {
    CompilationUnit generate();

    void setPackage(String Pack);

    void setEntityUnit(CompilationUnit entityUnit);

}
