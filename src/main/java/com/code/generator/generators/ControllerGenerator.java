package com.code.generator.generators;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import java.util.Optional;

import java.util.logging.Logger;

public class ControllerGenerator extends AbstractGenerator {

    private CompilationUnit _ServiceUnit;

    Logger log = Logger.getLogger("ServiceGenerator");

    public ControllerGenerator() {
        setPrefix("");
        setSuffix("Ctrl");
    }

    @Override
    public CompilationUnit generate() {
        String EntityPackage = getClassPackage(getEntityUnit());
        String EntityName = getClassName(getEntityUnit());
        String IdType = getIdType(getEntityUnit());
        IdType = IdType.substring(0, 1).toUpperCase() + IdType.substring(1);

        CompilationUnit cu;
        cu = new CompilationUnit();

        cu.setPackageDeclaration(getPackage());

        cu.addImport("org.springframework.beans.factory.annotation.Autowired");
        cu.addImport("org.springframework.web.bind.annotation.RequestMapping");
        cu.addImport("org.springframework.web.bind.annotation.RestController");
        cu.addImport("org.springframework.data.domain.Page");
        cu.addImport("org.springframework.data.domain.Pageable");
        cu.addImport("java.util.Optional");
        cu.addImport("org.springframework.web.bind.annotation.GetMapping");
        cu.addImport("org.springframework.web.bind.annotation.PostMapping");
        cu.addImport("org.springframework.web.bind.annotation.RequestBody");
        cu.addImport("java.util.List");
        cu.addImport(EntityPackage + "." + EntityName);
        cu.addImport(getClassPackage(_ServiceUnit) + "." + getClassName(_ServiceUnit));

        ClassOrInterfaceDeclaration aClass = cu.addClass(getPrefix() + EntityName + getSuffix(), Modifier.Keyword.PUBLIC);

        aClass.addAndGetAnnotation("RestController");
        NormalAnnotationExpr an = aClass.addAndGetAnnotation("RequestMapping");
        an.addPair("value", "\"/" + EntityName.toLowerCase() + "\"");


        FieldDeclaration service = aClass.addField(getClassName(_ServiceUnit), "service", Modifier.Keyword.PRIVATE);
        service.addAnnotation("Autowired");
        
        aClass.addMethod("getById")
                .addParameter("@PathVariable " + IdType + " ", "id")
                .setType("Optional<" + EntityName + ">")
                .addAnnotation("GetMapping")
                .addModifier(Modifier.Keyword.PUBLIC)
                .getBody()
                .get()
                .addStatement("Optional<" + EntityName + "> opt = service.findById(id);")
                .addStatement("return opt.isPresent() ? opt.get() : null;");
        
       aClass.addMethod("getAll")
                .setType("List<" + EntityName + ">")
                .addAnnotation("GetMapping")
                .addModifier(Modifier.Keyword.PUBLIC)
                .getBody()
                .get()
                .addStatement("return service.findAll();");
       
       aClass.addMethod("save")
                .addParameter("@RequestBody " + EntityName + " ", "item")
                .setType(EntityName)
                .addAnnotation("PostMapping")
                .addModifier(Modifier.Keyword.PUBLIC)
                .getBody()
                .get()
                .addStatement("return service.save(item);");
       
        return cu;
    }

    public void setServiceUnit(CompilationUnit serviceUnit) {
        _ServiceUnit = serviceUnit;
    }


}
