package com.code.generator.generators;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.type.TypeParameter;

public class GenericServiceGenerator extends AbstractGenerator {

    public GenericServiceGenerator() {
        setPrefix("I");
    }

    @Override
    public CompilationUnit generate() {

        CompilationUnit cu;
        cu = new CompilationUnit();

        cu.setPackageDeclaration(getPackage());

        cu.addImport("java.util.List");
        cu.addImport("java.util.Optional");

        ClassOrInterfaceDeclaration aClass = cu.addInterface(getPrefix() + "Generic" + getSuffix(), Modifier.Keyword.PUBLIC);
        TypeParameter tp = new TypeParameter();
        tp.setName("T");
        aClass.addTypeParameter(tp);
        tp = new TypeParameter();
        tp.setName("K");
        aClass.addTypeParameter(tp);

        aClass.addMethod("get")
                .addParameter("K", "id")
                .setType("Optional<T>")
                .removeBody();

        aClass.addMethod("getPage")
                .addParameter("Pageable", "pageable")
                .setType("Page<T>")
                .removeBody();

        aClass.addMethod("getAll")
                .setType("List<T>")
                .removeBody();

        aClass.addMethod("save")
                .addParameter("T", "entity")
                .setType("T")
                .removeBody();

        aClass.addMethod("removeById")
                .addParameter("K", "id")
                .removeBody();

        return cu;
    }
}
