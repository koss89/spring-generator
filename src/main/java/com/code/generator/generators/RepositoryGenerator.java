package com.code.generator.generators;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;

import java.util.logging.Logger;

public class RepositoryGenerator extends AbstractGenerator {

    Logger log = Logger.getLogger("ServiceGenerator");

    public RepositoryGenerator() {
        setPrefix("R");
        setSuffix("");
    }

    @Override
    public CompilationUnit generate() {
        String EntityPackage = getClassPackage(getEntityUnit());
        String EntityName = getClassName(getEntityUnit());
        String IdType = getIdType(getEntityUnit());
        IdType = IdType.substring(0, 1).toUpperCase() + IdType.substring(1);

        CompilationUnit cu;
        cu = new CompilationUnit();
        cu.setPackageDeclaration(getPackage());
        cu.addImport("org.springframework.data.jpa.repository.JpaRepository");
        cu.addImport("org.springframework.stereotype.Repository");
        cu.addImport(EntityPackage + "." + EntityName);
        ClassOrInterfaceDeclaration aClass = cu.addInterface(getPrefix() + EntityName + getSuffix(), Modifier.Keyword.PUBLIC);
        aClass.addAnnotation("Repository");
        aClass.addExtendedType("JpaRepository<"+EntityName+", "+IdType+">");

        return cu;
    }


}
