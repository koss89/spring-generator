package com.code.generator.generators;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;

import java.util.logging.Logger;

public class ServiceGenerator extends AbstractGenerator {

    Logger log = Logger.getLogger("ServiceGenerator");

    private CompilationUnit _GenericUnit;

    public ServiceGenerator() {
        setPrefix("I");
        setSuffix("Service");
    }


    @Override
    public CompilationUnit generate() {

        String EntityPackage = getClassPackage(getEntityUnit());
        String EntityName = getClassName(getEntityUnit());
        String IdType = getIdType(getEntityUnit());
        IdType = IdType.substring(0, 1).toUpperCase() + IdType.substring(1);

        CompilationUnit cu;
        cu = new CompilationUnit();
        cu.setPackageDeclaration(getPackage());

        cu.addImport(EntityPackage + "." + EntityName);

        ClassOrInterfaceDeclaration aClass = cu.addInterface(getPrefix() + EntityName + getSuffix(), Modifier.Keyword.PUBLIC);

        log.info("genName=" + getClassName(_GenericUnit));
        aClass.addExtendedType(getClassName(_GenericUnit) + "<" + EntityName + ", " + IdType + ">");

        return cu;
    }

    public void setGenericUnit(CompilationUnit genericUnit) {
        _GenericUnit = genericUnit;
    }
}
