package com.code.generator.generators;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;

public class ImplServiceGenerator extends AbstractGenerator {

    private CompilationUnit _ServiceUnit;
    private CompilationUnit _RepoUnit;

    public ImplServiceGenerator(String prefix, String suffix) {
        setPrefix(prefix);
        setSuffix(suffix);
    }

    @Override
    public CompilationUnit generate() {

        String EntityPackage = getClassPackage(getEntityUnit());
        String EntityName = getClassName(getEntityUnit());
        String IdType = getIdType(getEntityUnit());
        IdType = IdType.substring(0, 1).toUpperCase() + IdType.substring(1);

        CompilationUnit cu;
        cu = new CompilationUnit();

        cu.setPackageDeclaration(getPackage());

        cu.addImport("org.springframework.data.domain.Page");
        cu.addImport("org.springframework.data.domain.Pageable");
        cu.addImport("java.util.Optional");
        cu.addImport("java.util.List");
        cu.addImport(EntityPackage + "." + EntityName);
        cu.addImport(getClassPackage(_RepoUnit) + "." + getClassName(_RepoUnit));
        cu.addImport(getClassPackage(_ServiceUnit) + "." + getClassName(_ServiceUnit));

        ClassOrInterfaceDeclaration aClass = cu.addClass(getPrefix() + EntityName + getSuffix(), Modifier.Keyword.PUBLIC);
        aClass.addField(getClassName(_RepoUnit), "repository", Modifier.Keyword.PRIVATE);
        aClass.addAnnotation("Service");
        aClass.addImplementedType(getClassName(_ServiceUnit));

        aClass.addMethod("get")
                .addModifier(Modifier.Keyword.PUBLIC)
                .addParameter(IdType, "id")
                .setType("Optional<" + EntityName + ">")
                .addAnnotation("Override")
                .getBody()
                .get()
                .addAndGetStatement("return repository.findById(id)");

        aClass.addMethod("getPage")
                .addModifier(Modifier.Keyword.PUBLIC)
                .addParameter("Pageable", "pageable")
                .setType("Page<T>")
                .addAnnotation("Override")
                .getBody()
                .get()
                .addAndGetStatement("return repository.findAll(pageable)");

        aClass.addMethod("getAll")
                .addModifier(Modifier.Keyword.PUBLIC)
                .setType("List<" + EntityName + ">")
                .addAnnotation("Override")
                .getBody()
                .get()
                .addAndGetStatement("return repository.findAll()");

        aClass.addMethod("save")
                .addModifier(Modifier.Keyword.PUBLIC)
                .addAnnotation("Override")
                .addParameter(EntityName, "entity")
                .setType(EntityName)
                .getBody()
                .get()
                .addAndGetStatement("return repository.save(entity)");

        aClass.addMethod("removeById")
                .addModifier(Modifier.Keyword.PUBLIC)
                .addParameter(EntityName, "id")
                .addAnnotation("Override")
                .getBody()
                .get()
                .addAndGetStatement("repository.deleteById(id)");


        return cu;
    }

    public void setServiceUnit(CompilationUnit serviceUnit) {
        _ServiceUnit = serviceUnit;
    }

    public void setRepoUnit(CompilationUnit repoUnit) {
        _RepoUnit = repoUnit;
    }
}
