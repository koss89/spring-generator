package com.code.generator.generators;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;

import java.util.concurrent.atomic.AtomicReference;

public abstract class AbstractGenerator implements IGenerator {

    private String _Prefix = "";
    private String _Suffix = "";
    private String _Pack;
    private CompilationUnit _EntityUnit;


    public String getClassName(CompilationUnit cu) {
        AtomicReference<String> ClassName = new AtomicReference<>("");
        cu.findAll(ClassOrInterfaceDeclaration.class).stream()
                .forEach(f -> ClassName.set(f.getNameAsString()));
        return ClassName.get();
    }

    String getClassPackage(CompilationUnit cu) {
        return cu.getPackageDeclaration().get().getName().asString();
    }

    String getIdType(CompilationUnit cu) {
        AtomicReference<String> IdType = new AtomicReference<>("");
        cu.findAll(FieldDeclaration.class).stream()
                .forEach(f ->
                        f.asFieldDeclaration().getAnnotationByName("Id").ifPresent(el ->
                                IdType.set(f.getVariable(0).getType().asString()))
                );
        return IdType.get();
    }

    public String getPrefix() {
        return _Prefix;
    }

    public void setPrefix(String Prefix) {
        this._Prefix = Prefix;
    }

    public String getSuffix() {
        return _Suffix;
    }

    public void setSuffix(String Suffix) {
        this._Suffix = Suffix;
    }


    public CompilationUnit getEntityUnit() {
        return _EntityUnit;
    }

    @Override
    public void setEntityUnit(CompilationUnit entityUnit) {
        this._EntityUnit = entityUnit;
    }

    public String getPackage() {
        return _Pack;
    }

    @Override
    public void setPackage(String pack) {
        _Pack = pack;
    }
}
