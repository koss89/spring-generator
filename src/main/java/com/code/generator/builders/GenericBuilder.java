package com.code.generator.builders;

import com.code.generator.generators.GenericServiceGenerator;
import com.code.generator.generators.IGenerator;
import com.github.javaparser.ast.CompilationUnit;

public class GenericBuilder implements IBuilder {

    private IGenerator _generator;

    private GenericBuilder() {
    }

    public static GenericBuilder getInstance() {
        return new GenericBuilder();
    }

    @Override
    public GenericBuilder createGenerator() {
        _generator = new GenericServiceGenerator();
        return this;
    }

    @Override
    public GenericBuilder setEntityUnit(CompilationUnit EntityUnit) {
        _generator.setEntityUnit(EntityUnit);
        return this;
    }

    @Override
    public GenericBuilder setPackage(String pack) {
        _generator.setPackage(pack);
        return this;
    }


    @Override
    public IGenerator build() {
        return _generator;
    }

    @Override
    public CompilationUnit generate() {
        return _generator.generate();
    }


}
