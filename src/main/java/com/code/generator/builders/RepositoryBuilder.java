package com.code.generator.builders;

import com.code.generator.generators.IGenerator;
import com.code.generator.generators.RepositoryGenerator;
import com.github.javaparser.ast.CompilationUnit;

public class RepositoryBuilder implements IBuilder {

    private IGenerator _generator;

    private RepositoryBuilder() {
    }

    public static RepositoryBuilder getInstance() {
        return new RepositoryBuilder();
    }

    @Override
    public RepositoryBuilder createGenerator() {
        _generator = new RepositoryGenerator();
        return this;
    }

    @Override
    public RepositoryBuilder setEntityUnit(CompilationUnit EntityUnit) {
        _generator.setEntityUnit(EntityUnit);
        return this;
    }

    @Override
    public RepositoryBuilder setPackage(String pack) {
        _generator.setPackage(pack);
        return this;
    }


    @Override
    public IGenerator build() {
        return _generator;
    }

    @Override
    public CompilationUnit generate() {
        return _generator.generate();
    }


}
