package com.code.generator.builders;

import com.code.generator.generators.IGenerator;
import com.code.generator.generators.ImplServiceGenerator;
import com.github.javaparser.ast.CompilationUnit;

public class ImplServiceBuilder implements IBuilder {

    private ImplServiceGenerator _generator;

    private ImplServiceBuilder() {
    }

    public static ImplServiceBuilder getInstance() {
        return new ImplServiceBuilder();
    }

    @Override
    public ImplServiceBuilder createGenerator() {
        _generator = new ImplServiceGenerator("S", "Impl");
        return this;
    }

    @Override
    public ImplServiceBuilder setEntityUnit(CompilationUnit entityUnit) {
        _generator.setEntityUnit(entityUnit);
        return this;
    }

    public ImplServiceBuilder setServiceUnit(CompilationUnit serviceUnit) {
        _generator.setServiceUnit(serviceUnit);
        return this;
    }

    public ImplServiceBuilder setRepoUnit(CompilationUnit repoUnit) {
        _generator.setRepoUnit(repoUnit);
        return this;
    }

    @Override
    public ImplServiceBuilder setPackage(String pack) {
        _generator.setPackage(pack);
        return this;
    }


    @Override
    public IGenerator build() {
        return _generator;
    }

    @Override
    public CompilationUnit generate() {
        return _generator.generate();
    }


}
