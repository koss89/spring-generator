package com.code.generator.builders;

import com.code.generator.generators.IGenerator;
import com.github.javaparser.ast.CompilationUnit;

public interface IBuilder {

    IGenerator build();

    CompilationUnit generate();

    IBuilder setEntityUnit(CompilationUnit EntityUnit);

    IBuilder createGenerator();

    IBuilder setPackage(String pack);

}
