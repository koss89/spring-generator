package com.code.generator.builders;

import com.code.generator.generators.IGenerator;
import com.code.generator.generators.ServiceGenerator;
import com.github.javaparser.ast.CompilationUnit;

public class ServiceBuilder implements IBuilder {

    private ServiceGenerator _generator;

    private ServiceBuilder() {
    }

    public static ServiceBuilder getInstance() {
        return new ServiceBuilder();
    }

    @Override
    public ServiceBuilder createGenerator() {
        _generator = new ServiceGenerator();
        return this;
    }

    @Override
    public ServiceBuilder setEntityUnit(CompilationUnit EntityUnit) {
        _generator.setEntityUnit(EntityUnit);
        return this;
    }

    public ServiceBuilder setGenericUnit(CompilationUnit unit) {
        _generator.setGenericUnit(unit);
        return this;
    }

    @Override
    public ServiceBuilder setPackage(String pack) {
        _generator.setPackage(pack);
        return this;
    }


    @Override
    public IGenerator build() {
        return _generator;
    }

    @Override
    public CompilationUnit generate() {
        return _generator.generate();
    }


}
