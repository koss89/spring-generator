package com.code.generator.builders;

import com.code.generator.generators.ControllerGenerator;
import com.code.generator.generators.IGenerator;
import com.github.javaparser.ast.CompilationUnit;

public class ControllerBuilder implements IBuilder {

    private ControllerGenerator _generator;

    private ControllerBuilder() {
    }

    public static ControllerBuilder getInstance() {
        return new ControllerBuilder();
    }

    @Override
    public ControllerBuilder createGenerator() {
        _generator = new ControllerGenerator();
        return this;
    }

    @Override
    public ControllerBuilder setEntityUnit(CompilationUnit EntityUnit) {
        _generator.setEntityUnit(EntityUnit);
        return this;
    }

    @Override
    public ControllerBuilder setPackage(String pack) {
        _generator.setPackage(pack);
        return this;
    }

    public ControllerBuilder setServiceUnit(CompilationUnit serviceUnit) {
        _generator.setServiceUnit(serviceUnit);
        return this;
    }


    @Override
    public IGenerator build() {
        return _generator;
    }

    @Override
    public CompilationUnit generate() {
        return _generator.generate();
    }


}
