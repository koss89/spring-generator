package com.code.generator.builders;

import com.github.javaparser.ast.CompilationUnit;

public class FullBuilder {

    private String _packName;
    private String _BasePackage;

    public static FullBuilder getInstance() {
        return new FullBuilder();
    }

    public FullBuilder setPackName(String packName) {
        this._packName = packName;
        return this;
    }

    public FullBuilder setBasePackage(String basePackage) {
        _BasePackage = basePackage;
        return this;
    }

    public CompilationUnit genRepository(CompilationUnit EntityUnit) {
        return RepositoryBuilder.getInstance()
                .createGenerator()
                .setEntityUnit(EntityUnit)
                .setPackage(_packName)
                .generate();
    }

    public CompilationUnit genGenericService() {
        return GenericBuilder.getInstance()
                .createGenerator()
                .setPackage(_BasePackage + "." + _packName)
                .generate();
    }

    public CompilationUnit genService(CompilationUnit EntityUnit, CompilationUnit GenericUnit) {
        return ServiceBuilder.getInstance()
                .createGenerator()
                .setEntityUnit(EntityUnit)
                .setGenericUnit(GenericUnit)
                .setPackage(_BasePackage + "." + _packName)
                .generate();
    }

    public CompilationUnit genServiceImpl(CompilationUnit EntityUnit, CompilationUnit ServiceUnit, CompilationUnit RepoUnit) {
        return ImplServiceBuilder.getInstance()
                .createGenerator()
                .setEntityUnit(EntityUnit)
                .setServiceUnit(ServiceUnit)
                .setRepoUnit(RepoUnit)
                .setPackage(_BasePackage + "." + _packName + ".impl")
                .generate();
    }

    public CompilationUnit genCtrl(CompilationUnit EntityUnit, CompilationUnit ServiceUnit) {
        return ControllerBuilder.getInstance()
                .createGenerator()
                .setEntityUnit(EntityUnit)
                .setServiceUnit(ServiceUnit)
                .setPackage(_BasePackage + "." + _packName)
                .generate();
    }
}
